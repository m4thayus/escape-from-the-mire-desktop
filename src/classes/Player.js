class Player {
    constructor(id, name, charClass){
        // Basic Attributes
        this.id = id
        this.name = name
        this.charClass = charClass

        // Player Stats
        this.health = this.charClass === 'paladin' ? 80 : 50
        this.status = null
        this.score = 0

        // Player Position
        this.y = null
        this.x = null
        this.kublaiY = null
        this.kublaiX = null

        // Player Movement
        this.moveX = 0
        this.moveY = 0

        // Player Vision
        this.vision = {}
    }

    // Roll d20
    static roll(mob) {
        let min = Math.ceil(0);
        let max = Math.floor(20);
        let roll = Math.ceil(Math.random() * (max - min)) + min;
        if (!!mob) {
            roll = mob.type === "skeleton" ? roll + 2 : roll;
            roll = mob.type === "lich" ? roll - 5 : roll;
        } 
        roll = roll < 1 ? 1 : roll;
        roll = roll > 20 ? 20 : roll;
        return roll;
    }

    // Player Vision Updates
    updateFog(){
       let normalVision = {
            start: [this.x, this.y],
            top: [this.x,this.y-1],
            topRight: [this.x+1,this.y- 1],
            right: [this.x+ 1,this.y],
            bottomRight: [this.x+1,this.y+1],
            bottom: [this.x,this.y+1],
            bottomLeft: [this.x-1,this.y+1],
            left: [this.x-1,this.y],
            topLeft: [this.x-1,this.y-1],
            topLeftFar: [this.x-2,this.y-2],
            topLeftCenterFar: [this.x-1,this.y-2],
            topCenterFar: [this.x,this.y-2],
            topCenterFar: [this.x,this.y-2],
            topRightFar: [this.x+2,this.y-2],
            topRightCenterFar: [this.x+1,this.y-2],
            rightTopFar: [this.x+2,this.y-1],
            rightFar: [this.x+2,this.y],
            rightBottomFar: [this.x+2,this.y+1],
            bottomRightFar: [this.x+2,this.y+2],
            bottomRightCenter: [this.x+1,this.y+2],
            leftCenterTop: [this.x-2,this.y-1],
            leftBottomCenter: [this.x-2,this.y+1],
            bottomLeftFar: [this.x-2,this.y+2],
            bottomLeftCenter: [this.x-1,this.y+2],
            leftFar: [this.x-2,this.y],
            bottomCenterFar: [this.x,this.y+2],

        }
        
        // Ranger Vision
        let extendedVision = {
            topLeftSuperFar: [this.x-3, this.y-2],
            topLeftCenterSuperFar: [this.x-1, this.y-3],
            topCenterSuperFar: [this.x, this.y-3],
            topRightCenterSuperFar: [this.x+1, this.y-3],
            topRightSuperFar: [this.x+2, this.y-3],
            rightTopSuperFar: [this.x+3, this.y-1],
            rightSuperFar: [this.x+3, this.y],
            rightBottomSuperFar: [this.x+3, this.y+1],
            bottomRightSuperFar: [this.x+3, this.y+2],
            bottomRightCenterSuper: [this.x+1, this.y+3],
            bottomCenterSuperFar: [this.x, this.y+3],
            bottomLeftCenterSuper: [this.x-1, this.y+3],
            bottomLeftSuperFar: [this.x-3, this.y+3],
            leftBottomCenterSuper: [this.x-3, this.y+1],
            leftSuperFar: [this.x-3, this.y],
            leftCenterTopSuper: [this.x-3, this.y-1],
            bottomLeftCenterSuperFar: [this.x-2, this.y+3],
            bottomRightCenterSuperFar: [this.x+2, this.y+3],
            bottomRightSuperFarCorner: [this.x+3, this.y+3],
            leftBottomCenterSuperFar: [this.x-3, this.y+2],
            topLeftSuperFarCorner: [this.x-3, this.y-3],
            topLeftCenterSuperFarCorner: [this.x-2, this.y-3],
            topRightSuperFarCorner: [this.x+3, this.y-3],
            rightTopCenterSuperFar: [this.x+3, this.y-2]
        }

        // if (this.charClass === 'ranger') {
        //     this.vision = {...normalVision, ...extendedVision}
        // } else {
            this.vision = normalVision
        // }
    }
    
    // Player Movement Handler
    movement(event) {
        this.moveX = 0
        this.moveY = 0
        switch (event.key){
            case 'ArrowUp':
            case 'w':
                this.moveY = -1
                this.validateMovement()
                break;
            case 'ArrowDown':
            case 's':
                this.moveY = 1
                this.validateMovement()
                break;
            case 'ArrowLeft':
            case 'a':
                this.moveX = -1
                this.validateMovement()
                break;
            case 'ArrowRight':
            case 'd':
                this.moveX = 1
                this.validateMovement()
                break;
        }
    }

    // Handle Dynamic Monster Collisions
    checkMonsterCollision() {
        let mob = Monster.all.find(monster => monster.y === this.y && monster.x === this.x)
        if (!!mob) {
            let direction = null
            let move = null
            while (!(mob.validMove(direction, move))) {
                direction = Math.random() < 0.5 ? "y" : "x"
                move = Math.random() < 0.5 ? -1 : 1;
            }
            mob[direction] += move
            let roll = Player.roll(mob);
            console.log("Roll:", roll)
            
            if (roll === 1) {
                this.status = "splat"
                this.health = 0;
                console.log(`A ${mob.type} killed ${this.name}!`)
                updateConsole(`A ${mob.type} killed ${this.name}!`) 
            } else if (roll === 20) {
                mob.status = "splat"
                this.score += 100
                console.log(`${this.name} killed a ${mob.type}!`)
                updateConsole(`${this.name} killed a ${mob.type}!`) 
                console.log(`Kublai licks ${this.name}'s wounds...`)
                updateConsole(`Kublai licks ${this.name}'s wounds...`) 
                console.log(`${this.name} feels a little better!`)
                updateConsole(`${this.name} feels a little better!`) 
                this.health += 10
            } else if (roll >= 10) {
                mob.status = "splat"
                this.score += 100
                console.log(`${this.name} killed a ${mob.type}!`)
                updateConsole(`${this.name} killed a ${mob.type}!`)
                if (this.charClass === 'necromancer') {
                    this.health += 5
                    console.log(`...and absorbs the ${mob.type}'s energy!`)
                    updateConsole(`...and absorbs the ${mob.type}'s energy!`)
                    showHealth().innerText = `Health: ${this.health}`
                } 
            } else {
                console.log(`A ${mob.type} hit ${this.name}!`)
                updateConsole(`A ${mob.type} hit ${this.name}!`) 
                this.score += 50
                if (
                    this.charClass === "rogue" && 
                    Math.random() < 0.33
                    ) {
                    console.log(`But ${this.name} dodges the attack!`)
                    updateConsole(`But ${this.name} dodges the attack!`)
                } else {
                    this.status = "splat"
                    this.health -= 10;
                }
            }
            console.log("Health:", this.health)
            console.log("Score:", this.score)
            showHealth().innerText = `Health: ${this.health}`
            currentScore().innerText = `Current Score: ${this.score}`
            return !!mob
        }
    }

    // Handle Static Tile Collisions
    checkStaticCollision() {
        let collision = false
        if (level[this.y][this.x].texture === 'chest') {
            console.log(`${this.name} found a treasure chest...`)
            updateConsole(`${this.name} found a treasure chest...`) 
            if (this.charClass === 'sage') {
                this.health += 30
                console.log(`It heals ${this.name}!`)
                updateConsole(`It heals ${this.name}!`)
                console.log("Health:", this.health)
                showHealth().innerText = `Health: ${this.health}`
            }
            this.score += 300
            level[this.y][this.x].texture = null
            console.log("Score:", this.score)
            currentScore().innerText = `Current Score: ${this.score}`
            collision = true
        }
        if (level[this.y][this.x].texture === 'blood') {
            console.log(`${this.name} steps through a pool of blood...`)
            updateConsole(`${this.name} steps through a pool of blood...`) 
            level[this.y][this.x].texture = 'tentacle'
            console.log(`a tentacle appears and attacks ${this.name}...`)
            updateConsole(`a tentacle appears and attacks ${this.name}...`)
            let roll = Player.roll();
            if (roll !== 20) {
                console.log(`the tentacle hits ${this.name}!`)
                updateConsole(`the tentacle hits ${this.name}!`)
                this.health -= 10
                console.log("Health:", this.health)
                showHealth().innerText = `Health: ${this.health}`
                this.status = "splat"
            } else {
                console.log(`the tentacle misses ${this.name}!`)
                updateConsole(`the tentacle misses ${this.name}!`)
            }
            this.score -= 50
            console.log("Score:", this.score)
            currentScore().innerText = `Current Score: ${this.score}`
            collision = true
        }
        return collision
    }

    // Handle Wall Collisions and Player Death
    validateMovement(){
        if (this.health <= 0) {
            alert(`You died. ${messageArray[Math.floor(Math.random() * messageArray.length)]}`)
            this.updatePlayer()
            window.location.reload()
            return 
        }
        if (
            (this.y + this.moveY === exit.y && this.x + this.moveX === exit.x) ||
            (this.y + this.moveY === entrance.y && this.x + this.moveX === entrance.x)
            )  {

            this.kublaiY = this.y
            this.kublaiX = this.x
            this.x += this.moveX
            this.y += this.moveY

            // Show the player move into exit
            levelObj.generateMap()

            // Update score and send to backend
            this.score += 500
            this.updatePlayer()

            // Clear monsters 
            Monster.all = Monster.all.filter(monster => Math.random() < 0.25)
    
            // Make new level
            level = new Level(30, 30)
            cells = level.cells

            // Show tiles where player spawns
            this.updateVision()	
        
            // Reset status and heal player
            this.health += 50;
            this.status = null;
            showHealth().innerText = `Health: ${this.health}`
            currentScore().innerText = `Current Score: ${this.score}`

            // Show new map
            level.generateMap()

        } else if (level[this.y + this.moveY][this.x + this.moveX].type != 'wall') {
            this.status = null
            this.kublaiY = this.y
            this.kublaiX = this.x
            this.x += this.moveX
            this.y += this.moveY

            // Update tiles that player can see 
            this.updateFog()	

            //Check for static collisions and re-render map
            if (this.checkStaticCollision()) {
                // re-render map
                level.generateMap()
            }

            // Give the mobs a turn to move and re-render map
            Monster.takeTurn(); 

            // Check for mob collision 
            if (this.checkMonsterCollision()) {
                // re-render map
                level.generateMap()
            }

            level.generateMap()
        }		
    }
}

export default Player